package com.example.localstack_sample

import org.socialsignin.spring.data.dynamodb.repository.EnableScan
import org.springframework.data.repository.CrudRepository


@EnableScan
interface ItemInfoRepository : CrudRepository<ItemInfo, String> {
    fun findFirstByItemName(itemName: String): ItemInfo?
    fun deleteByItemName(itemName: String)
}
