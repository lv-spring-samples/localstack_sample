package com.example.localstack_sample

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
@EnableDynamoDBRepositories
class AwsDynamoDbConfig(
        @Value("\${config.aws.region}")
        private val region: String,

        @Value("\${config.aws.dynamodb.url}")
        private val dynamoDbEndpointUrl: String,

        @Value("\${config.aws.dynamodb.access-key}")
        private val accessKey: String,

        @Value("\${config.aws.dynamodb.secret-key}")
        private val secretKey: String,
) {
    companion object {
        val log: Logger = LoggerFactory.getLogger(AwsDynamoDbConfig::class.java)
    }

    private val basicAWSCredentials = BasicAWSCredentials(accessKey, secretKey)
    private val credentialsProvider = AWSStaticCredentialsProvider(basicAWSCredentials)

    @Bean
    fun amazonDynamoDB(): AmazonDynamoDB {
        log.info("region: $region")
        log.info("dynamoDbEndpointUrl: $dynamoDbEndpointUrl")
        log.info("accessKey: $accessKey")
        log.info("secretKey: $secretKey")
        return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(credentialsProvider)
                .withEndpointConfiguration(EndpointConfiguration(dynamoDbEndpointUrl, region))
                .build()
    }
}
