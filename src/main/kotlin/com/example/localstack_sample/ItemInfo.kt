package com.example.localstack_sample

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable


@DynamoDBTable(tableName = "ItemInfo")
data class ItemInfo (
    @DynamoDBAutoGeneratedKey
    @DynamoDBHashKey(attributeName = "id")
    var id: String? = null,

    @DynamoDBAttribute(attributeName = "itemName")
    var itemName: String? = null,

    @DynamoDBAttribute(attributeName = "itemUrl")
    var itemUrl: String? = null,
)
