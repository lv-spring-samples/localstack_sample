package com.example.localstack_sample

import com.jayway.jsonpath.JsonPath
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.MediaTypes
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ItemInfoRepositoryTest(
        @Autowired
        private val mockMvc: MockMvc,
        @Autowired
        private val itemInfoRepository: ItemInfoRepository,
) {

    @BeforeAll
    fun cleanDb() {
        itemInfoRepository.deleteAll()
    }

    @Test
    fun `should get first itemInfo by name`() {
        val itemInfo = ItemInfo(null, "test", "www.test.com")
        itemInfoRepository.save(itemInfo)

        mockMvc.get("/itemInfoes/search/findFirstByItemName?itemName=test")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaTypes.HAL_JSON) }
                    jsonPath("$.itemName") { value(itemInfo.itemName) }
                    jsonPath("$.itemUrl") { value(itemInfo.itemUrl) }
                }
    }

    @Test
    fun `should post itemInfo and then get first by name`() {
        val itemName = "post-test"
        val postResult = mockMvc.post("/itemInfoes") {
            contentType = MediaType.APPLICATION_JSON
            accept = MediaTypes.HAL_JSON
            content = """{
                |"itemName": "post-test",
                |"itemUrl": "www.post-test.com"
                |}""".trimMargin()
        }.andExpect {
            status { isCreated() }
            content { contentType(MediaTypes.HAL_JSON) }
            jsonPath("$.itemName") { value(itemName) }
            jsonPath("$.itemUrl") { value("www.$itemName.com") }
        }.andReturn().response.contentAsString

        val postResultId = JsonPath.read<String>(postResult, "$._links.self.href").split("/").last()

        mockMvc.get("/itemInfoes/$postResultId")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaTypes.HAL_JSON) }
                    jsonPath("$.itemName") { value(itemName) }
                    jsonPath("$.itemUrl") { value("www.$itemName.com") }
                }

        mockMvc.get("/itemInfoes/search/findFirstByItemName?itemName=$itemName")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaTypes.HAL_JSON) }
                    jsonPath("$.itemName") { value(itemName) }
                    jsonPath("$.itemUrl") { value("www.$itemName.com") }
                }
    }

}
