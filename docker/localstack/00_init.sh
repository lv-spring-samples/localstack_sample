#!/bin/bash

# -- > Create DynamoDb Table
echo Creating  DynamoDb \'ItemInfo\' table ...
awslocal dynamodb create-table --cli-input-json '{
  "TableName": "ItemInfo",
  "KeySchema": [{"AttributeName":"id","KeyType":"HASH"}],
  "AttributeDefinitions": [{"AttributeName":"id","AttributeType":"S"}],
  "BillingMode": "PAY_PER_REQUEST"
}'

# --> List DynamoDb Tables
echo Listing tables ...
awslocal dynamodb list-tables
